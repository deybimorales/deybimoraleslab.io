Title: Jitsi Meet, la alternativa gratis a Zoom
Date: 2020-06-01
Category: HOME
Tags: videoconferencia, cursos
Slug: jitsi


<img src="https://i.pinimg.com/originals/ce/59/4a/ce594a2076cd27b11cb7b326b2a35e4e.png" alt="drawing" width="600"/>

**Jitsi Meet** es un software online para realizar videos conferencias que no necesita de descargas ni de instalación en la computadora, únicamente se necesita de un navegador web actualizado.
En caso que falle algún navegador como Chrome, probar abrir el enlace de la reunión en Mozilla o Internet Explorer.

**Ventajas de usar Jitsi Meet**.

1.	Es gratuito, pero sin límites. No contiene tiempo de pruebas para realizar una reunión. La sala de reunión creada puede estar abierta hasta 5 horas según pruebas.

2.	Multiplataforma. En una computadora únicamente es necesario abrir el enlace de la reunión creada. En el caso de utilizar un Smartphone, tanto en Android como en IOS (iphone), al abrir el enlace se solicitará instalar la versión móvil de Jitsi Meet.

3.	Para utilizar Jitsi Meet no es necesario crear una cuenta. Los usuarios pueden ingresar únicamente con el enlace de la reunión y contraseña del creador de la reunión. Para ser identificados cada usuario puede colocarse un “nombre”.

4.	A diferencia de ZOOM Meeting, las conversaciones de Jitsi Meeting están cifradas. No pueden ser intervenidas, ni recibir el ingreso de personas que no tengan el enlace de la sala y/o contraseña.

5.	Creado por Atlassian, una marca que crear software a nivel empresarial. Jitsi es la plataforma gratuita de 8x8, una versión empresarial.

Para empresas que no hacen uso extenso y exclusivo de videollamadas Jitsi Meet brinda básicamente todo lo necesario.

### Iniciar una videoconferencia

Para crear una sala de reunión se debe ingresar a la dirección web: https://meet.jit.si/ . 

<img src="https://i.pinimg.com/originals/bd/3f/89/bd3f8908e7355c8209c8e86531d3ce5d.png" alt="drawing" width="600"/>

Nombra la sala de reunión a crearse en “Start a new meeting”.

Un detalle a tomar en cuenta es que los nombres podrían ser escaso, hay que diferenciar con mayúsculas, minúsculas y números. Jitsi Meet advertirá si el nombre ya fue utilizado.
Por ejemplo, podremos nombrar la nueva reunión como “fogadePrueba4”.

<img src="https://i.pinimg.com/originals/87/84/3c/87843c44c0df7a7ed8c464fedafda57c.png" alt="drawing" width="600"/>

Dar clic en GO.

### Permitir micrófono.

Para tener una participación eficiente de Jitsi Meet es necesario contar con cámara y micrófono. 
Dar clic en permitir el uso del micrófono.

<img src="https://i.pinimg.com/originals/ad/64/26/ad6426ec0c8ee34184f8c1d3c0c66daf.png" alt="drawing" width="600"/>

*Aparece otra notificación ofreciéndonos instalar un complemento de Chrome o Mozilla para manejar el Calendario de Google y de Office. No es necesario, puede cerrarse en la esquina derecha superior dando clic en la x.*


<img src="https://i.pinimg.com/originals/c0/af/2d/c0af2d4b5fbdccb27672232b6177a321.png" alt="drawing" width="600"/>


Tendremos la salar en el navegador, aparece de esta forma:

<img src="https://i.pinimg.com/originals/94/e0/49/94e049c4b3f71414e254c4256c74d116.png" alt="drawing" width="600"/>

Si el micrófono no aparece tachado es que está funcionando correctamente. En caso de dar problemas podemos hacer los siguientes pasos.

Cliquear en la flecha hacía abajo del micrófono. Esto nos permite en el caso de que tengamos varios micrófonos conectados elegir uno específico. Cuando se utiliza una laptop y estamos utilizando unos audífonos con micrófonos, podemos elegir si utilizar el micrófono de la computadora o del audífono. De igual manera se pueden elegir los altavoces preferidos en el caso de tener varios conectados.

### Permitir cámara.

Se activa dando clic en el ícono de cámara. Además si contamos con varias cámaras se puede elegir una específica.

### Cambiar nombre de usuario.

Para se identificado es necesario activar la cámara o cambiar el nombre de usuario.

Ir al recuadro que diga “me”. Dar doble clic en “me”

<img src="https://i.pinimg.com/originals/01/16/fe/0116fee9277371b7cbb1b50232020ac5.png" alt="drawing" width="600"/>

### Evitar Ruido.

Si no se piensa hablar es necesario desactivar el micrófono de forma temporal para evitar que se mezcle el ruido de nuestro entorno en el audio. Se puede hacer dando clic al ícono del micrófono hasta que este esté tachado.

### Compartir Pantalla.

Cualquier usuario puedes compartir la pantalla de la computadora. 

Clic en el ícono en forma de monitor al lado izquierdo inferior de la pantalla. Se selecciona una de las opciones disponibles. Se puede compartir toda la pantalla o seleccionar la ventana del programa específico. 


============================================================================
============================================================================

***¿Te gusta leer ebooks?. Prueba Scribd el netflix de libros. Disponible para todos los dispositivos digitales: ebooks, audiolibros, documentos y revistas con una mensualidad de $8.99 mensuales pero si te registras desde la imagen podrás probarlo gratis durante dos meses. Aprovecha***


<a title="Scribd" href="https://es.scribd.com/g/b08lv" target="_blank" ><img src="https://i.pinimg.com/originals/aa/61/f6/aa61f6990db4dab6c2093d3197eb4315.png" alt="Scribd" width="200" /></a>
