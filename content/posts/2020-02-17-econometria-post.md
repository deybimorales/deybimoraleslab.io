Title: Base de Datos para Econometria 5ed Damodar Gujarati
Date: 2020-02-17
Category: HOME
Tags: econometria, recursos
Slug: econometria-post_a


Para los colegas que ya tienen en sus manos la 5 edición de libro de Econometría de Dámodar. Gujarati... Les traigo las bases de datos para los ejercicios planteados en el libro.

<img src="https://0.academia-photos.com/attachment_thumbnails/33282471/mini_magick20190404-13804-psakpq.png?1554409321" alt="drawing" width="200"/>


A diferencia de la anteriores tablas, estas vienen en formato excel:

<a href="https://ouo.io/jNGjSs" target="_blank">DESCARGAR</a>


Espero que les sea de utilidad para estudiar y enseñar econometría. 

Saludos

Lic. Deybi Morales León

morales.economia@gmail.com


<script type="text/javascript">
amzn_assoc_placement = "adunit0";
amzn_assoc_search_bar = "true";
amzn_assoc_tracking_id = "moralesecon06-20";
amzn_assoc_ad_mode = "manual";
amzn_assoc_ad_type = "smart";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "US";
amzn_assoc_title = "Mi Selección de Amazon";
amzn_assoc_linkid = "44478a7a6be42cf5292fe842e0601924";
amzn_assoc_asins = "7300150918,0073042099,0071276254,0073375772";
</script>
<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>

<script type="text/javascript">
var infolinks_pid = 38629;
var infolinks_wsid = 0;
</script>
<script type="text/javascript" src="//resources.infolinks.com/js/infolinks_main.js"></script>
