Title: BUSCARV y BUSCARH en Google Sheet o Excel
Date: 2020-07-03
Category: HOME
Tags: excel, googlesheets, cursos
Slug: googlesheets_a

En este momento vamos a ver la utilización de dos fórmulas muy importantes que todo usuario de una hoja de cálculo debería conocer. Estas son VLOOKUP (BUSCARV) y HLOOKUP(BUSCARH).

-¿Para qué sirven?

Sirven para buscar información específica en un cuadro cumpliendo una condición. Esta condición puede ser un texto o un número. Por ejemplo, en una enorme tabla buscar el número telefónico del presidente de una compañía. En una columna especificada extraerá el número teléfonico cumpliendo que sea el presidente de la compañía.

**Ejercicio: Tenemos que completar la siguiente tabla. Los grados y los créditos en base a los códigos de clases y GPA de la columna 2 y 4.**

<img src="https://i.pinimg.com/originals/b8/d0/38/b8d0388830f55df7797cf882c5cc3db6.png" alt="drawing" width="600"/>

En vez de hacerlo manual vamos a utilizar las fórmulas de buscar. 

La información que vamos a utilizar la vamos extraer de las tablas 2 y 3.

La tabla 2 posee los créditos por clases. Veamos que la columna 2 de la tabla a llenar, tiene los mismos códigos de la columna 1 de la tabla 2. La información está vertical por lo tanto la busqueda será con VLOOKUP o BUSCARV.

<img src="https://i.pinimg.com/originals/77/af/1d/77af1d78d2f74574d88f33954816efe8.png" alt="drawing" width="600"/>


Los elementos que poseen la fórmula es:


```excel
=VLOOKUP(search_key, range, index, is_sorted)
```

*search_key*: es el valor de búsqueda. En este caso será cada código que se buscará en la tabla 2.

*range*: es la tabla en la que está la información a extraer. Tabla 2. Seleccionamos todas las columnas y filas que compongan la tabla, por último fijamos con F4.

*index*: para VLOOKUP o BUSCARV es el número de la columna que posee en nuestro caso los créditos. Columna 2.

*is_sorted*: Se refiere a si están ordenados o no los datos. TRUE o FALSE, VERDADERO o FALSO.


Para poder llenar los créditos de clases en base al código nuestra fórmula es:

```excel
=BUSCARV(C3,$B$15:$D$26,2,FALSO)
```

Resultado:

<img src="https://i.pinimg.com/originals/e8/ac/62/e8ac623e7aacf9c85fc8c83b33334fea.png" alt="drawing" width="600"/>

Nuestro siguiente paso es llenar la columna Grado en base a los GPA. La tabla 3 posee la información que vamos a necesitar. Esta tabla está horizontal por lo tanto utilizaremos HLOOKUP o BUSCARH.

<img src="https://i.pinimg.com/originals/ba/60/bb/ba60bb644faf44527b47f4d466d629af.png" alt="drawing" width="600"/>

La composición de la fórmula es igual a la anterior. Pero la diferencia está en index, que este será el número de la fila en la que se encuentran los valores a extraer.

```excel
=HLOOKUP(search_key, range, index, is_sorted)
```

Nuestra fórmula queda:

```excel
=BUSCARH(E3,$B$29:$H$30,2,VERDADERO)
```

Verdadero porque los datos están ordenados.

Hemos completado la tabla 1.

<img src="https://i.pinimg.com/originals/f7/75/3f/f7753f9e5ecda3503ac4dd1e104e52c2.png" alt="drawing" width="600"/>

Buscar información o completar de manera manual ya es cosa del pasado gracias a BUSCARV o BUSCARH. Recordemos que aunque vemos estos ejemplos en GoogleSheets, estás mismas formulas y con igual comportamiento están en Microsoft Excel.

Si tiene dudas, con gusto leeré su comentario.

morales.economia@gmial.com
Deybi Morales León

============================================================================
============================================================================

***¿Te gusta leer ebooks?. Prueba Scribd el netflix de libros. Disponible para todos los dispositivos digitales: ebooks, audiolibros, documentos y revistas con una mensualidad de $8.99 mensuales pero si te registras desde la imagen podrás probarlo gratis durante dos meses. Aprovecha***


<a title="Scribd" href="https://es.scribd.com/g/b08lv" target="_blank" ><img src="https://i.pinimg.com/originals/aa/61/f6/aa61f6990db4dab6c2093d3197eb4315.png" alt="Scribd" width="200" /></a>






















