Title: Funciones básicas en Google Sheets y Excel
Date: 2020-06-09
Category: HOME
Tags: cursos
Slug: googlesheets1


<img src="https://i.pinimg.com/originals/ba/59/b5/ba59b538d21a34e49a5630c392a69f3a.png" alt="drawing" width="300"/>


<a href="https://www.google.com/intl/es/sheets/about/" target="_blank">Google Sheets</a> se ha convertido en una alternativa profesional a Excel de Microsoft.

Si no conoces Google Sheets es:

1. Online, necesitas un navegador e internet para usarlo, a pesar que posee complementos para su utilización sin internet, toda su capacidad es en las nubes. Microsoft Office también posee un Excel online pero hasta el momento no es tan completo como Google Sheets.

2. Su uso es gratuito si tienes una cuenta correo de gmail.

3. Todo lo que hagas en Google Sheets no ocupa espacio de la cuenta de Google Drive, la nube en la cual se aloja. La capacidad gratuita de Google Drive es de 15GB para guardar ocuparlo en otros tipos de archivos.

4. Tiene fórmulas parecidas a las de Excel, quizás todas las de Excel. Pero también posee otras fórmulas de las que Excel debería copiarse.

5. Facilita la publicación de resultados en Internet, algo útil cuando se crean Dashboard. Las gráficas poseen funciones de JavaScripts lo que las convierten en dinámicas y facilitan que sean embebidas en cualquier página web. 

6. Las hojas se pueden integrar a varios servicios de Google como Google Colaboratory, Datastudio y Google Cloud.

7. Algunas fórmulas permiten que las hojas de cálculos trabajen en la nube la 24 horas sin costos.

Veamos algunas funciones muy básicas de Google Sheet, la mayoría funciona en Excel pero con su variante en español.

La primera a aprender es **ROUND**. Esta función permite redondear una cifra con decimales a una cifra entera. Por ejemplo: 3.4543 al aplicarle **ROUND** se convierte en 3.

Si deseamos sumar ya sea por fila o columna utilizamos **SUM**. En el ejemplo que comparto se pueden ver la aplicación de SUMA. ¿Cuánto se desembolsaría alguien que comprara un litro de cada combustible en cada año de la muestra?

La función **MIN** es la obtención del mínimo, por ejemplo, de 2006 al 2019, el precio menor que costó la Gasolina Regula fue de 15.7 córdobas. El **MAX** máximo era de 32.1 córdobas.

Se puede obtener una jerarquía de mayor a menor o viceversa del máximo que alcanzó cada combustible. Esto es posible con la función **RANK** llamada en español JERARQUIA, por default el 1 es el precio mayor que pertenece al Gas Licuado por 25 libras, 2 el que sigue como combustible más caro del periodo (Gasolina Regular), al haber siete observaciones, el 7 le pertenece al menor de todos, el Fuel Oil Energía. 

Se puede cambiar el orden con un tercer argumento de la fórmula, el default 1 para que enumere de mayor a menor, 2 para que enumere de menor a mayor.

Ejemplo en la hoja de cálculo:
=JERARQUIA(C35, $C$23:$I$23, 1)

**AVERAGE**, es el promedio. En la hoja se calculan el precio promedio por combustible.

**MEDIAN**, es la mediana, es una función para conocer el valor que se encuentra en el centro de los valores ordenados de mayor a menor o viceversa.

Como plus tenemos la raíz cuadrada de cualquier número que se obtiene con **SQRT** o **RAIZ**.

Google Sheets tiene un excelente autocompletado de funciones y también traduce las fórmulas del inglés al idioma del Google Sheets que tengas configurado (No sucede los mismo con Excel, si las pones en inglés no se reconocerán, sin embargo, he puesto las traducciones para que las pruebes en Excel). 

<a href="https://docs.google.com/spreadsheets/d/1tuGXuXEeqirNeanOk7-xrHMEzo9J3UlqG767eM5HrdU/edit?usp=sharing
" target="_blank">DESCARGAR SHEET</a>

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRf0WM8IwhBuMSn79zIpwnBA8k0nm-vzsndqK5sTpx0jXg8TPM065aFLbieSbwWeMykLxcZQcDWpqsH/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false" width="600" height="600"></iframe>


***Deybi Morales León***

morales.economia@gmail.com

============================================================================
============================================================================

***¿Te gusta leer ebooks?. Prueba Scribd el netflix de libros. Disponible para todos los dispositivos digitales: ebooks, audiolibros, documentos y revistas con una mensualidad de $8.99 mensuales pero si te registras desde la imagen podrás probarlo gratis durante dos meses. Aprovecha***


<a title="Scribd" href="https://www.scribd.com/g/b08lv" target="_blank" ><img src="https://i.pinimg.com/originals/aa/61/f6/aa61f6990db4dab6c2093d3197eb4315.png" alt="Scribd" width="200" /></a>




