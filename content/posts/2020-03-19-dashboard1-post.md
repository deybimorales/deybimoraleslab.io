Title: Dashboard de Coronavirus en el Mundo
Date: 2020-03-19
Category: HOME
Tags: dashboard, coronavirus
Slug: dashboard1

Nos encontramos en cuarentena, temerosos o desinformados acerca del Covid-19 que está asotando al mundo este marzo 2020. Me puse a elaborar un dashboard con datos públicos de la Pandemia Coronavirus. Este Dashboard está elaborado utilizando tecnología de Google:

-- Google Sheets

-- DataStudio

Los datos provienen del peinado web a la página Worldometers que está agrupando información cada minuto de páginas oficiales como Organización Panamericana de la Salud y Organización Mundial de la Salud. 

Utilizando un truco con Google Sheets, la información en la base de datos montada en Google Drive se actualizará cada minuto que se presenten cambios en los datos originales. Mientras por defecto el dashboard automáticamente se alimentará de Google Sheets cada 15 minutos sin la intervención del visitante.

Espero que sea de utilidad informativa. Hay aspecto positivo como el alto nivel de recuperación de los contagiados por el coronavirus pero también negativos como las edades más castigadas por el virus y el alto nivel de contagio lo cual está colapsando los sistemas de salud del mundo.

Si quieres dar un seguimiento aquí comparto el Dashboard:

[DASHBOARD FULL](https://datastudio.google.com/reporting/032f942a-96dc-4aaf-8193-2396479658ca?fbclid=IwAR31yqfT97dYnMgIbVUM0jwaonI5gQTo9DU0kWjXAAiVd6ei0AFba4ZjEIc)

<iframe width="700" height="1500" src="https://datastudio.google.com/embed/reporting/032f942a-96dc-4aaf-8193-2396479658ca/page/9VtIB" frameborder="0" style="border:0" allowfullscreen></iframe>


<script type="text/javascript">
amzn_assoc_placement = "adunit0";
amzn_assoc_search_bar = "true";
amzn_assoc_tracking_id = "moralesecon06-20";
amzn_assoc_ad_mode = "manual";
amzn_assoc_ad_type = "smart";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "US";
amzn_assoc_title = "Mi Selección de Amazon";
amzn_assoc_linkid = "0184c7909421a77e6187e310ebe4b71b";
amzn_assoc_asins = "1119616085,3836260972,B01NBHMZI9,B01IMV1NH2";
</script>
<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>


[morales.economia@gmail.com](morales.economia@gmail.com)
