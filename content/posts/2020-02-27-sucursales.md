Title: Cobertura Bancaria en Nicaragua 2019
Date: 2020-02-27
Category: HOME
Tags: bancos
Slug: bancos_post_a


En Nicaragua tenemos 7 bancos comerciales. Me surgía la pregunta, ¿En qué partes del país tienen presencia estos bancos? Ignorando los cajeros automáticos o puntos fáciles que encontramos en los supermercados, minisupers y gasolineras, este es un cuadro que presenta ¿Cuántas sucursales o ventanillas tiene cada banco en los departamentos de Nicaragua?. Este tipo de presencia facilita al cliente el acceso a los servicios bancarios completos.

Esta actualización es con datos de 2019 obtenidos de la página web de la *Superintendencia de Bancos y de Otras Instituciones Financieras - SIBOIF.*

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0pky">Institución / Departamento</th>
    <th class="tg-0pky">BANCO ATLÁNTIDA NICARAGUA, S.A.</th>
    <th class="tg-0pky">BANCO AVANZ, S.A.</th>
    <th class="tg-0pky">BANCO DE AMERICA CENTRAL, S.A.</th>
    <th class="tg-0pky">BANCO DE FINANZAS, S.A.</th>
    <th class="tg-0pky">BANCO DE LA PRODUCCION, S.A.</th>
    <th class="tg-0pky">BANCO FICOHSA NICARAGUA, S.A.</th>
    <th class="tg-0pky">BANCO LAFISE BANCENTRO, S.A.</th>
    <th class="tg-0pky">TOTALES</th>
  </tr>
  <tr>
    <td class="tg-0pky">Boaco</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">8</td>
  </tr>
  <tr>
    <td class="tg-0pky">Carazo</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">10</td>
  </tr>
  <tr>
    <td class="tg-0pky">Chinandega</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">9</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">11</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">25</td>
  </tr>
  <tr>
    <td class="tg-0pky">Chontales</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">12</td>
  </tr>
  <tr>
    <td class="tg-0pky">Estelí</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">16</td>
  </tr>
  <tr>
    <td class="tg-0pky">Granada</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">10</td>
  </tr>
  <tr>
    <td class="tg-0pky">Jinotega</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">7</td>
  </tr>
  <tr>
    <td class="tg-0pky">León</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">12</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">22</td>
  </tr>
  <tr>
    <td class="tg-0pky">Madriz</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">4</td>
  </tr>
  <tr>
    <td class="tg-0pky">Managua</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">59</td>
    <td class="tg-0pky">12</td>
    <td class="tg-0pky">72</td>
    <td class="tg-0pky">9</td>
    <td class="tg-0pky">32</td>
    <td class="tg-0pky">190</td>
  </tr>
  <tr>
    <td class="tg-0pky">Masaya</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">19</td>
  </tr>
  <tr>
    <td class="tg-0pky">Matagalpa</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">10</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">7</td>
    <td class="tg-0pky">26</td>
  </tr>
  <tr>
    <td class="tg-0pky">Nueva Segovia</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">9</td>
  </tr>
  <tr>
    <td class="tg-0pky">RACCN</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">7</td>
  </tr>
  <tr>
    <td class="tg-0pky">RACCS</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">7</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky">15</td>
  </tr>
  <tr>
    <td class="tg-0pky">Rivas</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">17</td>
  </tr>
  <tr>
    <td class="tg-0pky">Río San Juan</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">3</td>
  </tr>
  <tr>
    <td class="tg-0pky">TOTALES</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">12</td>
    <td class="tg-0pky">115</td>
    <td class="tg-0pky">22</td>
    <td class="tg-0pky">153</td>
    <td class="tg-0pky">14</td>
    <td class="tg-0pky">82</td>
    <td class="tg-0pky">400</td>
  </tr>
</table>

Este cuadro llama la atención en los siguientes aspectos:

**Banco con mayor cobertura:** *Banpro* con 153 sucursales y/o ventanillas. Gana por el mayor número de sucursales y/o ventanillas bancarias, seguido del *BAC* con 115 y en tercer lugar las 82 sucursales y/o ventanillas de *Lafise Bancentro*. Haciendo un balance estos tres bancos comerciales que son los más grandes de Nicaragua son los que cubren a todo el país.

*Banpro* se encuentra en todos los departamentos, pero *BAC* no tiene sucursales y/o ventanillas en Río San Juan y *Lafise Bancentro* no tiene cobertura en el departamento de Madriz.


**Banco con menor cobertura:** *Banco Atlántida* que únicamente se encuentra en Managua. Podemos justificarlo ya que empezó a operar desde noviembre del año 2019. Le sigue en orden el *Avanz*, que anteriormente era *Banco Procredit*, tiene muchos años con nosotros pero se ha quedado estancado con la cobertura nacional. El tercer lugar con menor cobertura le pertenecen a los 14 sucursales y/o ventanillas que posee el Banco de origen hondureño, *Banco Ficohsa*.

*BDF* tiene 22 sucursales y/o ventanillas. 

La poca presencia de *BDF*, *Banco Ficohsa* y *Avanz* también se reflejaba en los pocos cajeros automáticos. Por lo tanto decidieron formar una alianza llamada *BancaRed* para utilizar cajeros automáticos compartidos con la finalidad que sus clientes pudieran hacer transacciones de los tres bancos en una sola máquina. Una excelente idea que ocupa menos espacios y consume menos energía en los comercios en los cuales se han ubicados estos cajeros. El resto de instituciones bancarias deberían adoptar esta iniciativa.

<img src="https://s3-us-west-2.amazonaws.com/s3.laprensa.com.ni-bq/wp-content/uploads/2019/06/24153623/IMG_6445-2.jpeg.jpeg" alt="drawing" width="300"/>


**Departamento con mayor cobertura Bancaria:** Managua, posee 190 sucursales y/o ventanillas. Desde 2 sucursales y/o ventanillas del *Banco Atlántida* hasta 72 sucursales y/o ventanillas de Banpro. Le siguen de lejos Matagalpa 26, Chinandega 25 y León 22.

Los departamentos de Nicaragua con poca presencia bancaria son Río San Juan 3 y Madriz 4 sucursales y/o ventanillas. Solo *Banpro* y *Lafise Bancentro* tienen sucursales y/o ventanillas en Río San Juan. 

Los departamentos que tienen sucursales y/o ventanillas de todos los bancos excepto al *Banco Atlántida* son: Chinandega, Estelí, León, Masaya y Matagalpa. Es de destacar, que a Chontales y a Rivas les hace falta el *Banco Ficohsa*. Carazo y Granada no tienen a *Avanz* ni al *Banco Ficohsa*. Nueva Segovia no tiene al *Banco Ficohsa* ni al *BDF* en su territorio.

--------------------------------------------------------------------------------------------------------

En la actualidad esta información ya no es tan decisiva para elegir un banco. Si eres de los clientes que únicamente hacen uso de servicios básicos como depositar o debitar, los bancos de Nicaragua han aumentado el alcance territorial de estos servicios distribuyendo cajeros automáticos en puntos estratégicos del país, teniendo presencia con **Puntos Fáciles**, **Puntos Express** y otros convenios con diferentes establecimientos comerciales, en los cuales el cliente puede desde pagar tarjeta de crédito, depositar dinero y hasta retirar dinero en efectivo con solo brindar su número de cuenta. Llama la atención que en estos comercios el servicio bancario tiene horarios extendidos en dependencia de la hora de cierre del establecimiento. A experiencia personal Lafise Bancentro deja de utilizar estos puentes a partir de las 4:30 pm.

El internet también ha sido de ayuda con servicios como Transferencias interbancarias ACH, con lo cual el cliente con acceso a internet y a la página web del banco de preferencia puede transferir dinero a cualquier otro banco.


Si eres un cliente que necesitas los servicios bancarios avanzados y viajas mucho al interior del país o vives en el interior del país espero que esta información te sea de utilidad.


Lic. Deybi Morales León

morales.economia@gmail.com


*La anterior información fue extraída y filtrada con [Python](https://www.python.org), quizás te interese algunos libros para aprender ¿cómo?:*

<script type="text/javascript">
amzn_assoc_placement = "adunit0";
amzn_assoc_search_bar = "true";
amzn_assoc_tracking_id = "moralesecon06-20";
amzn_assoc_ad_mode = "manual";
amzn_assoc_ad_type = "smart";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "US";
amzn_assoc_title = "Mi Selección de Amazon";
amzn_assoc_linkid = "d6f6a7720ab50a45668dd71a0e48f00e";
amzn_assoc_asins = "1491957662,1492041130,1654982199,1530051126";
</script>
<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>