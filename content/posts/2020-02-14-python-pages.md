Title: Hacer histograma con Python
Date: 2020-02-14
Category: HOME
Tags: python
Slug: python-histograma-pages

Una de las gráficas más solicitadas para un análisis preliminar de los datos son los histogramas para presenciar cómo están distribuidas las observaciones de una variable. Visualizar si existe una distribución normal o están sesgados.


![](https://i.pinimg.com/originals/9a/9a/be/9a9abefdc0df2a810668d6f640ae8e63.png "")

Python es un lenguaje de programación multifuncional que últimamente se ha dado un espacio en las estadísticas gracias al interés de las Ciencias de Datos. Gracias a esto podemos hacer un histograma de manera gratuita a diferencia de otros programas que no son gratis como Python, por ejemplo: SPSS, Stata, Eviews, por mencionar los más importantes.

Si quieres hacer un histograma, el primer paso es tener los datos:

```python
from sklearn.datasets import load_iris
dataset=load_iris()
import pandas as pd
```
El paquete Sklearn trae consigo la base de datos famosa llamada Iris. Un poco más de GOOGLE de tu parte podrás saber de qué tratan esos datos. En este caso para no abarcar lo que ya se ha abarcado en otros blogs, lo único que explicaré es que las líneas anteriores son para extraer a nuestro Python esos datos que utilizaremos para el histograma. En tu caso puede usar cualquier otra base de datos.

```python
import pandas as pd
```
La anterior línea es para llamar a Pandas. Es un paquete que nunca debe faltar si vas a analizar datos con Python.

```python
data=pd.DataFrame(dataset['data'],columns=['Petal length','Petal Width','Sepal Length','Sepal Width'])
data.head()
```

Estamos dando la estructura de DataFrame a la base de datos extraída de Sklearn. Lo hacemos más legible con Pandas. Note también que hemos nombrado las columnas. Con head() obtenemos las primeras cinco observaciones:

|	|Petal length   |Petal Width	   |Sepal Length	 |Sepal Width|
| --|-------------  |:----------------:|:---------------:|----------:|
| 0	| 5.1	        | 3.5	           | 1.4	         | 0.2       |
| 1	| 4.9	        | 3.0              | 1.4	         | 0.2       |
| 2	| 4.7	        | 3.2	           | 1.3	         | 0.2       |
| 3	| 4.6	        | 3.1	           | 1.5	         | 0.2       |
| 4	| 5.0	        | 3.6	           | 1.4	         | 0.2       |

Una vez teniendo los datos en Python. Proseguimos directo a lo que venimos. Crear un histograma.

Deberemos importar Matplotlib que es otro paquete importante, el cual sirve para generar gráficos. Si lo complementamos con el paquete Seaborn podremos generar casi de manera automática todo tipo de gráficas estadísticas.

```python
import matplotlib.pyplot as plt
import seaborn as sns
```
Podremos graficar cualquier columna. Hagámoslo con “Petal length”:

```python
sns.set()
_ = plt.hist('Petal length', data=data)
plt.show()
```


![](https://i.pinimg.com/originals/7a/09/2f/7a092f2ebd40bbcb459f0269dcebd5b1.png "")


Podremos configurar este histograma para que se vea mejor.

1. Agregar etiquetas:

```python
sns.set()
_ = plt.hist('Petal length', data=data)
_ = plt.ylabel("count")
_ = plt.xlabel("petal length (cm)")
plt.show()
```

![](https://i.pinimg.com/originals/71/65/b6/7165b68d1e0b01b511a2e0de3e799c2c.png "")


2. Agregar título:

```python
sns.set()
_ = plt.hist('Petal length', data=data)
_ = plt.ylabel("count")
_ = plt.xlabel("petal length (cm)")
_ = plt.title("Histograma")
plt.show()
```

![](https://i.pinimg.com/originals/dc/ae/dd/dcaeddceef739dd8dab14b97bacc8368.png "")

3. Cambiar el número de barras o intervalos (bins). En este caso son diez. Configuremos a 20, por ejemplo:

```python
sns.set()
_ = plt.hist('Petal length', data=data, bins=20)
_ = plt.ylabel("count")
_ = plt.xlabel("petal length (cm)")
_ = plt.title("Histograma")
plt.show()
```

![](https://i.pinimg.com/originals/ac/70/fa/ac70fa65625d69c92bc9a5ba6e0c6ed1.png "")


Quizás tengas tu método para definir los bins. Por ejemplo:

```python
sns.set()

#Paquete para hacer operaciones matemáticas:
import numpy as np

#Número de observaciones
n_data = len(data)

# Raíz cuadrada de número de observaciones: n_bins
n_bins = np.sqrt(n_data)

# Convertir a valor entero: n_bins
n_bins = int(n_bins)

_ = plt.hist('Petal length', data=data, bins = n_bins)
_ = plt.ylabel("Frecuencia")
_ = plt.xlabel("petal length (cm)")
_ = plt.title("Histograma")
plt.show()
```

![](https://i.pinimg.com/originals/9a/9a/be/9a9abefdc0df2a810668d6f640ae8e63.png "")


12 bins.

Terminamos con esto. Nos vemos en otra publicación.

*Histograma, python, frecuencia, petal, número de observaciones, intervalo de clases, bins, estadísticas, programación.*

moraleseconomia@gmail.com


<script type="text/javascript">
amzn_assoc_placement = "adunit0";
amzn_assoc_search_bar = "true";
amzn_assoc_tracking_id = "moralesecon06-20";
amzn_assoc_ad_mode = "manual";
amzn_assoc_ad_type = "smart";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "US";
amzn_assoc_title = "Mi Selección de Amazon";
amzn_assoc_linkid = "d6f6a7720ab50a45668dd71a0e48f00e";
amzn_assoc_asins = "1491957662,1492041130,1654982199,1530051126";
</script>
<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>